
def identificar():
    global proposicion, Alias_abecedario
    proposicion = proposicion.replace(" ", "")
    proposicion = proposicion.replace("XNOR", "3")
    proposicion = proposicion.replace("NOR", "1")
    proposicion = proposicion.replace("NI", "1")
    proposicion = proposicion.replace("NAND", "4")
    proposicion = proposicion.replace("NOY", "4")
    proposicion = proposicion.replace("XOR", "2")
    proposicion = proposicion.replace("SIYSOLOSI", "<->")
    proposicion = proposicion.replace("SOLOSI", "<-")
    proposicion = proposicion.replace("ESLOMISMOQUE", "->")
    proposicion = proposicion.replace("PORLOTANTO", "->")
    proposicion = proposicion.replace("ESIGUALQUE", "->")
    proposicion = proposicion.replace("ESIGUALA", "->")
    proposicion = proposicion.replace("ESIGUAL", "->")
    proposicion = proposicion.replace("SEINFIERE", "->")
    proposicion = proposicion.replace("BICONDICIONAL", "<->")
    proposicion = proposicion.replace("CONDICIONAL", "->")
    proposicion = proposicion.replace("DOBLEIMPLICACION", "<->")
    proposicion = proposicion.replace("IMPLICACION", "->")
    proposicion = proposicion.replace("SIENTONCES", "->")
    proposicion = proposicion.replace("ENTONCES", "->")
    proposicion = proposicion.replace("SISOLOSI", "<->")
    proposicion = proposicion.replace("IFONLYIF", "<->")
    proposicion = proposicion.replace("EXOR", "2")
    proposicion = proposicion.replace("EOR", "2")
    proposicion = proposicion.replace("DISYUNCIONEXCLUSIVA","(+)")
    proposicion = proposicion.replace("DISYUNCION", "+")
    proposicion = proposicion.replace("EQUIVALENCIA", "<->")
    proposicion = proposicion.replace("EQUIVALEA", "<->")
    proposicion = proposicion.replace("EQUIVALE", "<->")
    proposicion = proposicion.replace("NEGAR", "~")
    proposicion = proposicion.replace("NEGACIONDE", "~")
    proposicion = proposicion.replace("UNION","+")
    proposicion = proposicion.replace("IF", "->")
    proposicion = proposicion.replace("AND", "^")
    proposicion = proposicion.replace("*", "^")
    proposicion = proposicion.replace(".", "^")
    proposicion = proposicion.replace(",", "^")
    proposicion = proposicion.replace("NOT", "~")
    proposicion = proposicion.replace("NO", "~")
    proposicion = proposicion.replace("MAS", "+")
    proposicion = proposicion.replace("PLUS", "+")
    proposicion = proposicion.replace("OR", "+")
    proposicion = proposicion.replace("!", "~")
    proposicion = proposicion.replace("|", "+")
    proposicion = proposicion.replace("V", "+")
    proposicion = proposicion.replace("O", "+")
    proposicion = proposicion.replace("Y", "^")
    proposicion = proposicion.replace("&", "^")
    proposicion = proposicion.replace("(+)", "2")
    proposicion = proposicion.replace("SI", "->")
    proposicion = proposicion.replace("<>", "<->")
    proposicion = proposicion.replace("<-->", "<->")
    proposicion = proposicion.replace("<=>", "<->")
    proposicion = proposicion.replace("<==>", "<->")
    proposicion = proposicion.replace("=", "->")
    proposicion = proposicion.replace("=>", "->")
    proposicion = proposicion.replace("[", "(")
    proposicion = proposicion.replace("]", ")")
    proposicion = proposicion.replace("}", ")")
    proposicion = proposicion.replace("{", "(")

    for i in range (len(proposicion)):
        if (proposicion[i] in Nombres_variables) and (proposicion[i] in Variables)==False: Variables.append(proposicion[i])
    proposicion = proposicion.replace("1", " NOR ")
    proposicion = proposicion.replace("2", " XOR ")
    proposicion = proposicion.replace("3", " XNOR ")
    proposicion = proposicion.replace("4", " NAND ")
    Variables.sort()
    for i in range(len(Variables)):
        Alias_abecedario = Alias_abecedario.replace(Variables[i],"")
    proposicion = Multiplicar_concatenar(proposicion)

def Multiplicar_concatenar(cadena):
    i = 0
    aux = len(cadena)
    while i<aux-1:
        if (cadena[i] in Variables) and cadena[i+1] in Variables:
            cadena = cadena.replace(cadena[i]+cadena[i+1],cadena[i]+"^"+cadena[i+1])
            i = 0
            aux = len(cadena)
        else: i += 1
    return cadena



def Mostrar_ejemplos():
    print """

        TODAS LAS SIGUIENTES ENTRADAS SON VALIDAS PARA EL PROGRAMA

        LEYES DE MORGAN:

            1   (no (A y B)) es lo mismo que (no A o no B)
            1   (p^q) -> (~pv~q)
            2   (no (A o B)) es lo mismo que (no A y no B)
            2   (pvq) -> (~p^~q)
            3   negar ( A v B) <-> ( negar a y negar b)
            3   ~(A+B)<->(~A^~B)


        DEDUCCION NATURAL:

            Introduccion a la negacion / Regla de inferencia
            1   ( p entonces q) y (p entonces negacion de q) se infiere negar p
            1   (P->Q)^(P->~Q)->~P

            Eliminacion de la negacion
            2   negar p se infiere ( p entonces r)
            2   ~P->(P->R)

            Elimacion de la doble negacion
            3   negar negar p se infiere p
            3   ~~P->P

            Introduccion de la negacion
            4   p,q se infiere ( p ^ q)
            4   P^Q->(P^Q)

            Eliminacion de la conjuncion
            5   ( p ^ q ) se infiere p
            5   (P^Q)->P
            6   ( p ^ q ) se infiere q
            6   (P^Q)->Q
            7   ((p y q) entonces p) y (( p y q) entonces q)
            7   ((P^Q)->P)^((P^Q)->Q)

            Introduccion a la disyuncion
            8   De p se infiere (p v q)
            8   P->(P+Q)
            9   De p se infiere ( p v q) y q se infiere ( p v q)
            9   P->(P+Q)^Q->(P+Q)

            Eliminacion de la disyuncion
            10  De (p v q) y ( p entonces r) y (q entonces r) se infiere r
            10  (P+Q)^(P->R)^(Q->R)->R

            Introduccion del Bicondicional
            11  (p y q) y (q y p) = (p<->p)
            11  (P^Q)^(Q^P)->(P<->P)

            Eliminacion del Bicondicional
            12  {(pvq),(p->r),(q->r)] = r
            12  ((P+Q)^(P->R)^(Q->R))->R

            Modus Ponens (Eliminacion del Condicional)
            13  p y (p -> q) se infiere q
            13 {p,(p->q)} = q
            13  P^(P->Q)->Q

            Prueba Condicional (Introduccion del Condicional)
            14  (p = q) = ( p ->q)
            14  (P->Q)->(P->Q)

        FORMAS DE ARGUMENTOS BASICAS Y DERIVADAS:

            Modus Ponens (Eliminacion del Condicional)
            15  p y (p -> q) se infiere q
            15  P^(P->Q)->Q

            Modus Tollens
            16  ((p ->q) ^p) = q

            Silogismo Hipotetico
            17  ((p -> q) ^ (q -> r)) = (p ->r)

            Silogismo Disyuntivo
            18  (( p o q) y negar p) por lo tanto q
            18  ((P+Q)^~P)->Q

            Dilema Constructivo
            19  ((p entonces q)  y (r entonces s) y (p or r)) entonces (q or s)
            19  ((P->Q)^(R->S)^(P+R))->(Q+S)

            Dilema Bidireccional
            20  ((p entonces q) y ( r entonces s) y (p v negar s)) es igual (q v negar r)
            20  ((P->Q)^(R->S)^(P+~S))->(Q+~R)

            Composicion
            21  (( p entonces q) y (p entonces r)) por lo tanto ( p entonces (q y r))
            21  ((P->Q)^(P->R))->(P->(Q^R))


        FORMAS NORMALES
            (P->Q)<->(~P+Q)
            (P<->Q)<->((~P+Q)^(~Q+P))

        EQUIVALENCIAS DE UN CONJUNTO COMPLETO

            Puertas NAND:
                1   a NAND a = negar a
                1   A NAND A->~A
                2   (a nand b) nand (a nand b) = a y b
                2   (A NAND B) NAND (A NAND B)->A^B
                3   (a nand a) nand (b nand b) = a o b
                3   (A NAND A) NAND (B NAND B)->A+B
                4   ((b NAND b) NAND (a NAND a)) NAND ( b nand b) = (a -> b)
                4   (B NAND B) NAND (A NAND A) NAND (B NAND B)->(A->B)

            Puertas NOR:
                1   a nor a = not a
                1   A NOR A->~A
                2   (a nor b) nor ( a nor b) = a o b
                2   (A NOR B) NOR (A NOR B)->A+B
                3   (a nor a) nor ( b nor b) = a y b
                3   (A NOR B) NOR (A NOR B)->A+B
                4   ((a nor a) nor b) nor ( (b nor b) nor a) = (a ->b)
                4   ((A NOR A) NOR B) NOR ((B NOR B) NOR A)->(A->B)

            Pseudo Asociatividad y Pseudo Distributividad de NOR y NAND
                1   A nor negar ( b nor c) equivale a negar ( a nor b) nor c
                1   A NOR ~(B NOR C)<->~(A NOR B) NOR C
                2   a nand negar (b nand c) equivale a negar( a nand b) nand c
                2   A NAND ~(B NAND C)<->~(A NAND B) NAND C
                3   a nor negar(b nand c) equivale (negar (a nor b) nand negacion de (a nor c))
                3   A NOR ~(B NAND C)<->(~(A NOR B) NAND ~(A NOR C))
                4   a nand negar ( b nor c) equivale a  negar (a nand b) nor negar(a nand c)
                4   A NAND ~(B NOR C)<->~(A NAND B) NOR ~(A NAND C)
    """



def TablaDeVerdad():
    print "\n\t\t\tTABLA DE VERDAD\n"
    print "\t",
    for i in range(len(Variables)):
        if Variables[i] in Lista_Alias: print "|", Lista_Alias.index(Variables[i])+1,
        else: print "|", Variables[i],
    print "|\n"
    for i in range(Tamanio+1):
        print "\t",
        for j in range(len(Renglon[i])):
            if Variables[j] in Lista_Alias and (Lista_Alias.index(Variables[j])+1) >= 10:
                if j == 0: print "|",
                print Renglon[i][j], " |",
            else:
                if j == 0: print "|",
                print Renglon[i][j], "|",
        print "\n"
        
def Funcion_Or(a,b):
    if a==1 or b==1: return 1
    else: return 0

def GenerarCombinaciones(size):
    aux = 0
    if size ==1:
        for i in range(2):
            Renglon[aux].append(i)
            aux += 1
    if size==2:
        for i in range(2):
           for j in range(2):
               Renglon[aux].append(i)
               Renglon[aux].append(j)
               aux += 1
    if size == 3:
        for i in range(2):
            for j in range(2):
                for k in range(2):
                    Renglon[aux].append(i)
                    Renglon[aux].append(j)
                    Renglon[aux].append(k)
                    aux += 1
    if size == 4:
        for i in range(2):
            for j in range(2):
                for k in range(2):
                    for l in range(2):
                        Renglon[aux].append(i)
                        Renglon[aux].append(j)
                        Renglon[aux].append(k)
                        Renglon[aux].append(l)
                        aux += 1
    if size == 5:
        for i in range(2):
            for j in range(2):
                for k in range(2):
                    for l in range(2):
                        for a in range(2):
                            Renglon[aux].append(i)
                            Renglon[aux].append(j)
                            Renglon[aux].append(k)
                            Renglon[aux].append(l)
                            Renglon[aux].append(a)
                            aux +=1
    if size == 6:
        for i in range(2):
            for j in range(2):
                for k in range(2):
                    for l in range(2):
                        for a in range(2):
                            for b in range(2):
                                Renglon[aux].append(i)
                                Renglon[aux].append(j)
                                Renglon[aux].append(k)
                                Renglon[aux].append(l)
                                Renglon[aux].append(a)
                                Renglon[aux].append(b)
                                aux += 1


def Operador_parentesis(cadena):
    global proposicion, contador
    global charx
    nuevaproposicion = ""
    if ("(" in cadena):
        for i in range(cadena.find("(")+1,len(cadena)):
            nuevaproposicion = nuevaproposicion + cadena[i]
        Operador_parentesis(nuevaproposicion)
    elif (")" in cadena):
        i = 0
        while cadena[i] != ")":
            nuevaproposicion = nuevaproposicion + cadena[i]
            i += 1
        Evaluar(nuevaproposicion)
        proposicion = proposicion.replace("(" + nuevaproposicion + ")", Alias_abecedario[contador])
        proposicion = Multiplicar_concatenar(proposicion)
        Alias[contador] = "(" + nuevaproposicion + ")"
        Operador_parentesis(proposicion)
    else: Evaluar(cadena)

def Evaluar(cadena):
    global contador
    aux = ""
    ok = 0
    if "~" in cadena:
        if cadena[cadena.find("~")+1] == "~":
            for it in range(len(cadena)):
                if cadena[it] == "~" and ok == 0:
                    aux += "!"
                    ok = 1
                else: aux += cadena[it]
            Evaluar(aux)
        else:
            for i in range(Tamanio):
                aux = Operador_Negar(Renglon[i][Variables.index(cadena[cadena.find("~") + 1])])
                Renglon[i].append(aux)
            contador += 1
            Alias[contador] = "~" + cadena[cadena.find("~") + 1]
            Lista_Alias.append(Alias_abecedario[contador])
            Variables.append(Alias_abecedario[contador])
            cadena = cadena.replace(Alias[contador], Alias_abecedario[contador])
            cadena = cadena.replace("!", "~")
            Evaluar(cadena)
    elif "^" in cadena:
        for i in range(Tamanio):
            aux = Operador_And(Renglon[i][Variables.index(cadena[cadena.find("^")-1])],Renglon[i][Variables.index(cadena[cadena.find("^")+1])])
            Renglon[i].append(aux)
        contador += 1
        Alias[contador] = cadena[cadena.find("^") - 1] + "^" + cadena[cadena.find("^") + 1]
        Proceso1(cadena)
    elif " NAND " in cadena:
        for i in range(Tamanio):
            aux = Operador_Nand(Renglon[i][Variables.index(cadena[cadena.find("N")-2])],Renglon[i][Variables.index(cadena[cadena.find("D")+2])])
            Renglon[i].append(aux)
        contador +=1
        Alias[contador] = cadena[cadena.find("N")-2]+" NAND "+cadena[cadena.find("D")+2]
        Proceso1(cadena)
    elif "+" in cadena:
        for i in range(Tamanio):
            aux = Operador_Or(Renglon[i][Variables.index(cadena[cadena.find("+")-1])],Renglon[i][Variables.index(cadena[cadena.find("+")+1])])
            Renglon[i].append(aux)
        contador +=1
        Alias[contador] = cadena[cadena.find("+")-1]+"+"+cadena[cadena.find("+")+1]
        Proceso1(cadena)
    elif " NOR " in cadena:
        for i in range(Tamanio):
            aux = Operador_Nor(Renglon[i][Variables.index(cadena[cadena.find("N")-2])],Renglon[i][Variables.index(cadena[cadena.find("R")+2])])
            Renglon[i].append(aux)
        contador +=1
        Alias[contador] = cadena[cadena.find("N")-2]+" NOR "+cadena[cadena.find("R")+2]
        Proceso1(cadena)

    elif " XNOR " in cadena:
        for i in range(Tamanio):
            aux = Operador_Xnor(Renglon[i][Variables.index(cadena[cadena.find("X")-2])],Renglon[i][Variables.index(cadena[cadena.find("R")+2])])
            Renglon[i].append(aux)
        contador +=1
        Alias[contador] = cadena[cadena.find("X")-2]+" XNOR "+cadena[cadena.find("R")+2]
        Proceso1(cadena)
    elif " XOR " in cadena:
        for i in range(Tamanio):
            aux = Operador_Xor(Renglon[i][Variables.index(cadena[cadena.find("X")-2])],Renglon[i][Variables.index(cadena[cadena.find("R")+2])])
            Renglon[i].append(aux)
        contador +=1
        Alias[contador] = cadena[cadena.find("X")-2]+" XOR "+cadena[cadena.find("R")+2]
        Proceso1(cadena)
    elif "<->" in cadena:
        for i in range(Tamanio):
            aux = Operador_Bicondicional(Renglon[i][Variables.index(cadena[cadena.find("<")-1])],Renglon[i][Variables.index(cadena[cadena.find(">")+1])])
            Renglon[i].append(aux)
        contador += 1
        Alias[contador] = cadena[cadena.find("<") - 1] + "<->" + cadena[cadena.find(">") + 1]
        Proceso1(cadena)
    elif "->" in cadena:
        for i in range(Tamanio):
            aux = Operador_Condicional(Renglon[i][Variables.index(cadena[cadena.find("-")-1])],Renglon[i][Variables.index(cadena[cadena.find(">")+1])])
            Renglon[i].append(aux)
        contador += 1
        Alias[contador] = cadena[cadena.find("-") - 1] + "->" + cadena[cadena.find(">") + 1]
        Proceso1(cadena)
    elif "<-" in cadena:
        for i in range(Tamanio):
            aux = Operador_ImplicacionOpuesta(Renglon[i][Variables.index(cadena[cadena.find("<")-1])],Renglon[i][Variables.index(cadena[cadena.find("-")+1])])
            Renglon[i].append(aux)
        contador += 1
        Alias[contador] = cadena[cadena.find("<") - 1] + "<-" + cadena[cadena.find("-") + 1]
        Proceso1(cadena)



def Proceso1(cadena):
    global contador
    Lista_Alias.append(Alias_abecedario[contador])
    Variables.append(Alias_abecedario[contador])
    cadena = cadena.replace(Alias[contador], Alias_abecedario[contador])
    Evaluar(cadena)

def Operador_ImplicacionOpuesta(x,y):
    if x == 0 and y == 1: return 0
    else: return 1

def Operador_Bicondicional(x,y):
    if (x == 1 and y == 1) or (x == 0 and y == 0): return 1
    else: return 0

def Operador_Xor(x,y):
    if (x == y): return 0
    else: return 1

def Operador_Xnor(x,y):
    if (x == y): return 1
    else: return 0

def Operador_Nor(x,y):
    if (x == 0 and y ==0): return 1
    else: return 0

def Operador_Condicional(x,y):
    if (x == 1 and y == 0): return 0
    else: return 1

def Operador_Negar(x):
    if x == 0: return 1
    if x == 1: return 0

def Operador_Or(x,y):
    if x== 1 or y == 1: return 1
    else: return 0

def Operador_And(x,y):
    if x ==1 and y ==1: return 1
    else: return 0

def Operador_Nand(x,y):
    if (x and y)==1: return 0
    else: return 1

def Arreglar_Alias():
    for i in range(len(Lista_Alias)):
        for j in range(len(Alias[i+1])):
            if Alias[i + 1][j] in Lista_Alias:
                Alias[i+1] = Alias[i+1].replace(Alias[i+1][j],Alias[Lista_Alias.index(Alias[i + 1][j])+1])
                Arreglar_Alias()

def Definir_Tipo():
    visor0 = 0
    visor1 = 0
    for i in range(Tamanio):
        if Renglon[i][len(Variables)-1] == 0: visor0 += 1
        elif Renglon[i][len(Variables)-1] == 1: visor1 += 1
    if visor0 == Tamanio: print "\t\tProposicion contradictoria / Contradiccion\n\t\tLa proposicion",Alias[len(Lista_Alias)].replace(" XOR ", "(+)"),"es falsa."
    elif visor1 == Tamanio: print "\t\tProposicion tautologica / Tautologia\n\t\tLa proposicion",Alias[len(Lista_Alias)].replace(" XOR ", "(+)"),"es verdadera."
    else: print "\t\tVerdad indeterminada / Contingencia\n\t\tLa proposicion",Alias[len(Lista_Alias)].replace(" XOR ", "(+)"),"puede ser falsa o verdadera."

def Main():
    global Tamanio, proposicion,proposicion_original
    proposicion =raw_input("\tIngresa la proposicion logica en su forma formal o informal:\n\t")
    proposicion_original = proposicion
    proposicion = proposicion.upper()
    if not("EJEM" in proposicion):
        identificar()
        Tamanio = 2 ** (len(Variables))
        for i in range(100):
            Renglon.append([])
            Alias.append([])
        GenerarCombinaciones(len(Variables))
        Operador_parentesis(proposicion)
        Arreglar_Alias()
        print "\n\tProposicion ingresada:\n\t", proposicion_original
        print "\n\tProposicion formalizada:\n\t", Alias[len(Lista_Alias)].replace(" XOR ", "(+)")
        print "\n\tNumero de combinaciones:", Tamanio
        print "\n\tConsidere:\n"
        for i in range(len(Lista_Alias)):
            print "\t\t", i + 1, "->", Alias[i + 1].replace(" XOR ", "(+)")
        TablaDeVerdad()
        Definir_Tipo()
    else:
        Mostrar_ejemplos()
        Main()

def Menu():
    try:
        Inicializar()
        Main()
        opc = raw_input("\n\tIngresar nueva proposicion:\n\t\t1.- Si\n\t\t2.- No\n\t\t")
        print "\n"
        if opc == "1": Menu()
    except:
        print "\tHa ocurrido un error, intente de nuevo\n\t","Revise: '",proposicion_original,"' al convertir a '",proposicion,"'"
        Menu()

def Inicializar():
    global Tamanio,contador,charx, Alias_abecedario,proposicion_original
    proposicion_original = ""
    Alias_abecedario = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTVUXYZ0123456789!#$"%&/()=?{}-.'
    while len(Variables)>0: Variables.pop()
    while len(Alias)>0: Alias.pop()
    while len(Lista_Alias)>0: Lista_Alias.pop()
    while len(Renglon)>0: Renglon.pop()
    Tamanio = contador = 0
    charx = ""


#VARIABLES GLOBALES
Nombres_variables = "ABCDEFGHIJKLMNPQRSTUXZ"
proposicion_original = ""
Alias_abecedario = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTVUXYZ0123456789#$"%&/()=?{}-.'
Variables = []
Alias = []
Lista_Alias = []
Operadores = []
Renglon = []
charx = ""
contador = 0
Tamanio = 0
proposicion = ""


print """
        Facultad de Ingenieria, UAEMex
        Programa desarrollado por Jovanny Ramirez Chimal

                        TABLA DE VERDAD DE UNA PROPOPOSICION LOGICA

        OPERADORES ACEPTADOS PARA:

            Parentesis  :   (), []
            Negacion    :   ~, !, not, no, negar, negacion de
            Conjuncion  :   ^, &, y, and, *, ., ,,por, conjuncion,
                            o concatene las proposiciones ejemplo:
                            (pq)(r+q) = (p^b)^(r + q)
            Disyuncion  :   +, v, mas, plus, |, or, o,
                            disyuncion, union
            Condicional :   ->, >, =>, =, if, si, se infiere,
                            entonces, condicional, implicacion,
                            es lo mismo que, por lo tanto
            Bicondicional:  <->, <>, <-->, <=>, <==>, si solo si,
                            si y solo si, if only if, equivalencia,
                            doble implicacion, bicondicional
            Disyuncion exclusiva:   (+), (mas), XOR, EOR, EOR
            Disyuncion opuesta:     NOR, ni
            Conjuncion Opuesta:     NAND, trazodeSheffer, Sheffer, no y,
            Implicacion Opuesta:    <-, solo si
            Puertas logicas:        AND, NAND, OR, XOR, XNOR, NOT, NOR

        #Nota:  Se pueden ocupar espacios, no hay distincion
                entre MAYUSCULAS y minusculas.
                Se admite un maximo de 6 variables.

        PARA VER EJEMPLOS ESCRIBA: Ejemplos
        
            """
Menu()
